# Steps performed


    1. npx create-next-app my-app
    2. npm add antd
    3. npm add -D babel-plugin-import
        install these packages as well 
        "@zeit/next-less": "^1.0.1",
        "antd": "^4.14.1",
        "babel-plugin-import": "^1.13.3",
        "less": "^4.1.1",
        "less-vars-to-js": "^1.3.0",
        "next": "10.0.9",
        "react": "17.0.2",
        "react-dom": "17.0.2",
        "webpack": "^4.44.2" 
    4. create a file name .babelrc in the root directory and paste following code
```javascript
{
    "presets": ["next/babel"],
    "plugins": [
      [
        "import",
        {
          "libraryName": "antd",
          "style": true
        }
      ]
    ]
  }
```

    5. 